Rails.application.routes.draw do
  
  devise_for :users, controllers: { sessions: 'users/sessions' }

  resources :users do
    member do
      post :enable_multi_factor_authentication, to: 'users/multi_factor_authentication#verify_enable'
      post :disable_multi_factor_authentication, to: 'users/multi_factor_authentication#verify_disabled'
    end
  end
  
  get  'static_pages/home'
  get  'static_pages/help'
  root 'static_pages#home'
end
